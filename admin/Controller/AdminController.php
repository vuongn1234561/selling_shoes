<?php
    include_once './Models/Admin.php';

    class AdminController extends Admin {
        public function __construct()
        {
            parent::__construct();

            $method = 'list';
            if (isset($_GET['method'])) {
                $method = $_GET['method'];
            }

            switch ($method) {
                case 'list':
                    $this->index();
                    break;

                case 'create':
                    $this->create();
                    break;

                case 'edit':
                    $this->edit();
                    break;

                case 'delete':
                    $this->delete();
                    break;

                default:
                    echo "Page 404 not found";
                    break;
            }
        }

        public function index()
        {
            $results = parent::index();
            include_once './pages/Products/view_admin.php';
        }

        public function create()
        {
            $categorys = parent::getCategory();
            $brands = parent::getBrand();
            if(isset($_REQUEST['create_product'])){
                $name = $_REQUEST['name'];
                $quantity = $_REQUEST['quantity'];
                $product_sold = $_REQUEST['product_sold'];
                $description = $_REQUEST['description'];
                $category = $_REQUEST['category'];
                $brand = $_REQUEST['brand'];
                $sale = $_REQUEST['sale'];
                $price = $_REQUEST['price'];
                $status = $_REQUEST['status'];
                $image = $_REQUEST['image'];
                parent::store($name,$quantity,$product_sold,$description,$category,$brand,$sale,$price,$status,$image);

            }
            include_once './pages/Products/add_admin.php';
        }

        public function edit(){
            $categorys = parent::getCategory();
            $brands = parent::getBrand();
            if(isset($_GET['id'])){
                $id = (int)$_GET['id'];
                $products = parent::getProductId($id);

                if(isset($_REQUEST['update_product'])){
                    $name = $_REQUEST['name'];
                    $quantity = $_REQUEST['quantity'];
                    $product_sold = $_REQUEST['product_sold'];
                    $description = $_REQUEST['description'];
                    $category = $_REQUEST['category'];
                    $brand = $_REQUEST['brand'];
                    $sale = $_REQUEST['sale'];
                    $price = $_REQUEST['price'];
                    $status = $_REQUEST['status'];
                    $image = $_REQUEST['image'];
                    parent::update($id,$name, $quantity, $product_sold, $description, $category, $brand, $sale, $price, $status, $image);
                }
            }
            include_once './pages/Products/edit_admin.php';
        }

        public function delete()
        {
            if(isset($_GET['id'])){
                $id = (int)$_GET['id'];
                parent::deleteProducts($id);
            }
            include_once './pages/Products/view_admin.php';
        }

    }
