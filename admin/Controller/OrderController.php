<?php
    include_once './Models/Order.php';

    class OrderController extends Order {
        public function __construct()
        {
            parent::__construct();

            $method = 'list';
            if (isset($_GET['method'])) {
                $method = $_GET['method'];
            }

            switch ($method) {
                case 'list':
                    $this->index();
                    break;

                default:
                    echo "Page 404 not found";
                    break;
            }
        }

        public function index()
        {
//            $results = parent::index();
            include_once './pages/Orders/view_orders.php';
        }
    }