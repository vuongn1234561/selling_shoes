<?php
    include_once '../config/Connect.php';

    class Admin extends Connect{
        public function __construct()
        {
            parent::__construct();
        }

        public function index(){
            $sql = "SELECT * FROM `products`;";
            $pre = $this->conn->prepare($sql);
            $pre->execute();
            $result = $pre->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        }

        public function store($name, $quantity, $product_sold, $description, $category_id, $brand_id, $sale, $price, $status, $image) {
            $sql = "INSERT INTO `products`(
    name,
    quantity,
    product_sold,
    description,
    category_id,
    brand_id,
    sale,
    price,
    STATUS,
    image
)
VALUES(
    :name,
    :quantity,
    :product_sold,
    :description,
    :category_id,
    :brand_id,
    :sale,
    :price,
    :status,
    :image
);";
            $pre = $this->conn->prepare($sql);
            $pre->bindParam(':name', $name);
            $pre->bindParam(':quantity', $quantity);
            $pre->bindParam(':product_sold', $product_sold);
            $pre->bindParam(':description', $description);
            $pre->bindParam(':category_id', $category_id);
            $pre->bindParam(':brand_id', $brand_id);
            $pre->bindParam(':sale', $sale);
            $pre->bindParam(':price', $price);
            $pre->bindParam(':status', $status);
            $pre->bindParam(':image', $image);
            $create = $pre->execute();

            if($create){
                echo "<script>alert('success!')</script>";
            }else{
                echo "<script>alert('fail!')</script>";
            }
        }

        public function update($id,$name, $quantity, $product_sold, $description, $category_id, $brand_id, $sale, $price, $status, $image) {
            $sql = "UPDATE
    `products`
SET
    name = '$name',
    quantity = '$quantity',
    product_sold = '$product_sold',
    description = '$description',
    category_id = $category_id,
    brand_id = $brand_id,
    sale = '$sale',
    price = '$price',
    status = '$status',
    image = '$image'
WHERE
    id = :id";
            $pre = $this->conn->prepare($sql);
            $pre->bindParam(':id',$id);
            $update = $pre->execute();

            if($update){
                echo "<script>alert('success!')</script>";
                header("Location: index.php?page=admin");
            }else{
                echo "<script>alert('fail!')</script>";
            }
        }

        public function getProductId($id)
        {
            $sql = "SELECT * FROM `products` WHERE id = :id";
            $pre = $this->conn->prepare($sql);
            $pre->bindParam(':id', $id);
            $pre->execute();
            return $pre->fetch(PDO::FETCH_ASSOC);
        }

        public function getBrand()
        {
            $sql = "SELECT id,name FROM `brand`;";
            $pre = $this->conn->prepare($sql);
            $pre->execute();
            $result = $pre->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        }

        public function getCategory()
        {
            $sql = "SELECT id,name FROM `category`";
            $pre = $this->conn->prepare($sql);
            $pre->execute();
            $result = $pre->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        }

        public function deleteProducts($id)
        {
            $sql = "DELETE FROM `products` WHERE id = :id";
            $pre = $this->conn->prepare($sql);
            $pre->bindParam(':id', $id);
            $del = $pre->execute();

            if($del){
                header("Location: index.php?page=admin");
                echo "<script>alert('success!')</script>";
            }else{
                echo "<script>alert('fail!')</script>";
            }
        }
    }