<div class="scrollbar-inner sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="assets/img/profile.jpg">
        </div>
        <div class="info">
            <a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                <span>
                    Hizrian
                    <span class="user-level">Administrator</span>
                    <span class="caret"></span>
                </span>
            </a>
            <div class="clearfix"></div>

            <div class="collapse in" id="collapseExample" aria-expanded="true" style="">
                <ul class="nav">
                    <li>
                        <a href="#profile">
                            <span class="link-collapse">My Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="#edit">
                            <span class="link-collapse">Edit Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="#settings">
                            <span class="link-collapse">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        <li class="nav-item active">
            <a href="index.php?page=admin">
                <p>Product</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="index.php?page=order&method=list">
                <p>Orders</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="forms.html">
                <p>Forms</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="tables.html">
                <p>Tables</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="notifications.html">
                <p>Notifications</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="typography.html">
                <p>Typography</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="icons.html">
                <p>Icons</p>
            </a>
        </li>
        <li class="nav-item update-pro">
            <button data-toggle="modal" data-target="#modalUpdate">
                <p>Update To Pro</p>
            </button>
        </li>
    </ul>
</div>