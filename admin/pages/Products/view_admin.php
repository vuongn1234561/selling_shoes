<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    List Product <br/>
                    <a href="?page=admin&method=create" class="btn btn-primary">Add product</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table mt-3 table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Sale</th>
                        <th scope="col">Price</th>
                        <th scope="col">Category</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $count = 0;
                    foreach ($results as $result) {
                    ?>
                        <tr>
                            <td><?= ++$count ?></td>
                            <td><?= $result['name'] ?></td>
                            <td><?= $result['quantity'] ?></td>
                            <td><?= $result['sale'] ?></td>
                            <td><?= number_format($result['price']); ?></td>
                            <td>
                                <?php
                                    $key = $result['category_id'];

                                    switch ($key){
                                        case 1:
                                            echo "Giày";
                                            break;
                                        case 2:
                                            echo "Áo";
                                            break;
                                        case 3:
                                            echo "Quần";
                                            break;
                                        default:
                                            echo "error";
                                            break;
                                    } ?>
                            </td>
                            <td>
                                <?php
                                $key = $result['brand_id'];

                                switch ($key){
                                    case 1:
                                        echo "Converse";
                                        break;
                                    case 2:
                                        echo "Nike";
                                        break;
                                    case 3:
                                        echo "Adidas";
                                        break;
                                    default:
                                        echo "error";
                                        break;
                                }
                                ?>
                            </td>
                            <td><?= $result['status'] ? "Nam" : "Nữ" ?></td>
                            <td>
                                <a href="index.php?page=admin&method=edit&id=<?= $result['id'] ?>" class="btn btn-primary">Edit</a>
                                <a href="index.php?page=admin&method=delete&id=<?= $result['id'] ?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>

                    <?php
                        }
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
