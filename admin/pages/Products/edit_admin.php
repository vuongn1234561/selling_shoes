<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Edit product</div>
            </div>

            <form action="" name="frm_create_student" method="POST">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" required class="form-control" name="name" id="name" value="<?= $products['name'] ?>" placeholder="Enter name">
                    </div>

                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="text" required class="form-control" name="quantity" id="quantity" value="<?= $products['quantity'] ?>" placeholder="Enter quantity">
                    </div>

                    <div class="form-group">
                        <label for="product_sold">Product Sold</label>
                        <input type="text" required class="form-control" name="product_sold" id="product_sold" value="<?= $products['product_sold'] ?>" placeholder="Enter product sold">
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" required class="form-control" name="description" id="description" value="<?= $products['description'] ?>" placeholder="Enter description">
                    </div>

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="category">
                            <?php
                            foreach ($categorys as $category) {
                                ?>
                                <option <?= ($category['id'] == $products['category_id']) ? 'selected' : '' ?> value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="brand">Brand</label>
                        <select class="form-control" id="brand" name="brand">
                            <?php
                            foreach ($brands as $brand) {
                                ?>
                                <option value="<?= $brand['id'] ?>"><?= $brand['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="sale">Sale</label>
                        <input type="text" required class="form-control" name="sale" id="sale" value="<?= $products['sale'] ?>" placeholder="Enter sale">
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" required class="form-control" name="price" id="price" value="<?= $products['price'] ?>" placeholder="Enter price">
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <input type="text" required class="form-control" name="status" id="status" value="<?= $products['status'] ?>" placeholder="Enter status">
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="text" required class="form-control" name="image" id="image" value="<?= $products['name'] ?>" placeholder="Enter image">
                    </div>


                </div>
                <div class="card-action">
                    <button class="btn btn-success" name="update_product" type="submit">Update</button>
                    <button class="btn btn-danger" type="reset" href="index.php?page=admin"><a href="index.php?page=admin">Cancel</a></button>
                </div>
            </form>
        </div>
    </div>
</div>
