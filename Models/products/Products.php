<?php
    include_once './config/Connect.php';

class Products extends Connect {
    public function __construct()
    {
        parent::__construct();
    }

    public function homePage() {
        $sql = 'SELECT
                    *
                FROM
                    `products`
                WHERE
                    products.status = 1  LIMIT 0,8';
        $pre = $this->conn->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_ASSOC);
    }

    public function commingProduct() {
        $sql = 'SELECT
                    *
                FROM
                    `products`
                WHERE
                    products.status = 0  LIMIT 0,8';
        $pre = $this->conn->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_ASSOC);
    }
}

