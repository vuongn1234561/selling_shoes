<?php
    include_once './config/Connect.php';

    class Cart extends Connect {
        public function __construct()
        {
            parent::__construct();
        }

        public function getProduct_id($id){
            $sql = 'SELECT
                        *
                    FROM
                        `products`
                    WHERE
                        id = :id';
            $pre = $this->conn->prepare($sql);
            $pre->bindParam(':id',$id);
            $pre->execute();
            return $pre->fetch(PDO::FETCH_ASSOC);

        }

    }