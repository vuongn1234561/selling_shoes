<?php
    include_once './Models/products/Products.php';

    class ProductController extends Products {

        public  function __construct()
        {
            parent::__construct();
            $this->homePage();

        }

//        Home Page
    public function homePage() {
        $results = parent::homePage();
        $commings = parent::commingProduct();

        include_once './pages/products/view_product.php';

    }
}