<?php
    include_once './Models/products/Cart.php';

    class CartController extends Cart {

        public  function __construct()
        {
            parent::__construct();
            $this->orderProduct();
        }

        public function orderProduct(){
            if(isset($_GET['id'])) {
                $id = (int)$_GET['id'];
                $order = parent::getProduct_id($id);
                if(!isset($_SESSION['carts']) && empty($_SESSION['carts'])){
                    $_SESSION['carts'][$id] = $order;
                    $_SESSION['carts'][$id]['qty'] = 1;
                } else {
                    if(array_key_exists($id, $_SESSION['carts'])){
                        $_SESSION['carts'][$id]['qty'] = $_SESSION['carts'][$id]['qty'] + 1;
                    }else{
                        $_SESSION['carts'][$id] = $order;
                        $_SESSION['carts'][$id]['qty'] = 1;
                    }
                }
            }

            if(isset($_REQUEST['update_cart'])){
                unset($_REQUEST['update_cart']);
                unset($_REQUEST['id']);
                unset($_REQUEST['page']);

                foreach ($_REQUEST as $id => $qty) {
                    $_SESSION['carts'][$id]['qty'] = $qty;
                }
            }



            include_once './pages/products/cart_product.php';
        }

    }