<?php
    include_once './Models/products/Confirmation.php';

    class ConfirmationController extends Confirmation {

        public  function __construct()
        {
            parent::__construct();
            $this->confirmation();
        }

        public function confirmation()
        {
            include_once "./pages/products/confirmation_product.php";
        }
    }