<?php
    include_once './Models/accounts/Logout.php';

    class LogoutController extends Logout {

        public  function __construct()
        {
            parent::__construct();
            $this->logout();
        }
        public function logout()
        {
            unset($_SESSION['login']);
            header("Location: index.php");
        }
    }

