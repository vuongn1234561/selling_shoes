<?php
    include_once './Models/accounts/Register.php';

    class RegisterController extends Register {

        public  function __construct()
        {
            parent::__construct();
            $this->register();

        }

        public function register() {
            include_once './pages/accounts/register.php';

        }
    }