<?php
    include_once './Models/accounts/Login.php';

    class LoginController extends Login {

        public  function __construct()
        {
            parent::__construct();
            $this->login();

        }

        public function login()
        {
            if(isset($_REQUEST['submit_login'])){
                $user =  $_REQUEST['user'];
                $pass =  $_REQUEST['pass'];
                $logins = parent::getAccount($user,$pass);
                foreach ($logins as $login){
                    if($user == $login['username'] && $pass == $login['password']){
                        $_SESSION['login'] = $login['id'];
                        echo "<script>alert('Đăng nhập thành công');</script>";
                    }else{
                        echo "<script>alert('Sai mật khẩu hoặc tài khoản');</script>";
                    }
                }
            }
            if(isset($_SESSION['login'])){
                header("Location: index.php");
            }
            include_once './pages/accounts/login.php';
        }
    }
