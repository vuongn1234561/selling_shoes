<!-- Start Main Top -->

<div class="main-top">
    <div class="container-fluid">
        <div class="header_row" style="height: 50px; padding: 0 10px">
            <div class="header_row_left">
            </div>
            <div class="header_row_right">
                <div class="custom-select-box">
                    <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
                        <option>¥ JPY</option>
                        <option>$ USD</option>
                        <option>€ EUR</option>
                    </select>
                </div>

                <div class="our-link">
                    <ul>
                        <li style="color: #bebbbb">
                            <li><a href="?page=login" id="login">Đăng nhập</a></li>
                            <li><a href="?page=register" id="register">Đăng ký</a></li>
                            <li><a href="?page=logout" id="logout">Đăng xuất</a></li>
                        </li>
                        <li><a href="#">Địa chỉ</a></li>
                        <li><a href="#">Liên hệ</a></li>
                        <li><a href="#">Hãy gọi cho chúng tôi: <a href="#"> +11 900 800 100</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main Top -->
<!--Start Header-->
<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.php"><img src="assets/img/logo/logo1.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Shop</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="source/category.html">Shop Category</a></li>
									<li class="nav-item"><a class="nav-link" href="index.php?page=billing">Product Checkout</a></li>
									<li class="nav-item"><a class="nav-link" href="index.php?page=order">Shopping Cart</a></li>
									<li class="nav-item"><a class="nav-link" href="source/confirmation.html">Confirmation</a></li>
								</ul>
							</li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Blog</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="source/blog.html">Blog</a></li>
									<li class="nav-item"><a class="nav-link" href="source/single-blog.html">Blog Details</a></li>
								</ul>
							</li>
							<li class="nav-item"><a class="nav-link" href="source/contact.html">Contact</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="index.php?page=cart" class="cart"><span class="ti-bag"></span></a></li>
							<li class="nav-item">
								<button class="search"><span class="lnr lnr-magnifier" id="search"></span></button>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="search_input" id="search_input_box">
			<div class="container">
				<form class="d-flex justify-content-between">
					<input type="text" class="form-control" id="search_input" placeholder="Search Here">
					<button type="submit" class="btn"></button>
					<span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
				</form>
			</div>
		</div>
	</header>