<?php
    if(!isset($_SESSION['login'])){
        header("location: index.php?page=login");
    }
?>
<section class="checkout_area section_gap">
    <div class="container">
        <form class="billing_details" action="" method="post">
            <div class="row">
                <div class="col-lg-8">
                    <h3>Thông tin nhận hàng</h3>
                        <div class="col-md-12 form-group p_star">
                            <input type="text" class="form-control " id="name" name="name" placeholder="Họ tên" required>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <input type="number" class="form-control" id="number" name="phone" placeholder="Số điện thoại" required>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <input type="text" class="form-control" id="add" name="address" placeholder="Địa chỉ" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea class="form-control" name="message" id="message" rows="5" placeholder="Ghi chú"></textarea>
                        </div>
                </div>
                <div class="col-lg-4">
                    <div class="order_box">
                        <h2>Đơn hàng</h2>
                            <ul class="list">

                                <li>
                                    <a href="#">Sản phẩm </a>
                                </li>
                                <?php
                                foreach ($_SESSION['carts'] as $value){
                                    ?>
                                <li>
                                    <a href="#"><?= $value['name']; ?>
                                        <span class="last">
                                            <span class="middle">x <?= $value['qty'] ?></span>
                                            <?php
                                            $item_sum = $value['price'] * $value['qty'];
                                            echo number_format($item_sum);
                                            ?><sup>đ</sup>
                                        </span></a>
                                </li>
                                        <?php
                                    }
                                ?>
                            </ul>
                            <ul class="list list_2">
                                <li>
                                    <a href="#">Tổng <span class="last" style="color: #ee4d2d !important;"><?php echo number_format($_SESSION['sum_price']);?><sup>đ</sup></span></a>
                                </li>
                            </ul>
                                <div class="">
                                    <input type="radio" name="selector" value="thanh_toan_khi_nhan_hang">
                                    <label for="f-option5">Thanh toán khi nhận hàng</label>
                                </div>
                                <div class="">
                                    <input type="radio" name="selector" value="paypal">
                                    <label for="f-option6">Paypal </label>
                                </div>
                            <button class="primary-btn" href="#" name="pay" style="width: 100%; border: none">Thánh toán</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>