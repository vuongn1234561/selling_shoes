<section class="order_details section_gap">
    <div class="container">
        <h3 class="title_confirmation">
            Cảm ơn bạn. Đơn đặt hàng của bạn đã được nhận.
        </h3>
        <div class="row order_d_inner">
            <div class="col-lg-12">
                <div class="details_item">
                    <h4>Thông tin đặt hàng</h4>
                    <ul class="list">
                        <li>
                            <a href="#"><span>Tên : </span><?= $_SESSION['name'] ?></a>
                        </li>
                        <li>
                            <a href="#"><span>Số điện thoại</span><?= $_SESSION['phone'] ?></a>
                        </li>
                        <li>
                            <a href="#"><span>Email : </span><?= $_SESSION['email'] ?></a>
                        </li>
                        <li>
                            <a href="#"><span>Địa chỉ : </span><?= $_SESSION['address'] ?></a>
                        </li>
                        <li>
                            <a href="#"><span>Ghi chú : </span><?= $_SESSION['message'] ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="order_details_table">
            <h2>Chi tiết đơn hàng</h2>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Sản phẩm</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Tổng cộng</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($_SESSION['carts'] as $value){

                    ?>
                    <tr>
                        <td>
                            <p><?= $value['name']; ?></p>
                        </td>
                        <td>
                            <h5>x <?= $value['qty']; ?></h5>
                        </td>
                        <td>
                            <p>
                                <?php
                                $item_sum = $value['price'] * $value['qty'];
                                echo number_format($item_sum);
                                ?><sup class="sup_new">đ</sup>
                            </p>
                        </td>
                    </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>
                            <h4>Tổng</h4>
                        </td>
                        <td>
                            <h5></h5>
                        </td>
                        <td>
                            <p><?= number_format($_SESSION['sum_price']) ?><sup class="sup_new">đ</sup></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
