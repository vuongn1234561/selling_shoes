<!--================Cart Area =================-->
<section class="cart_area">
    <div class="container mt-5">
        <div class="cart_inner">
            <div class="table-responsive">
                <form action="" method="post">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                            <th scope="col">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $_SESSION['sum_price'] = 0;
                        if(!empty($_SESSION['carts'])){
                            foreach ($_SESSION['carts'] as $value){
//                                    echo "<pre>";
//                                    print_r($value);
//                                    echo "</pre>";
                                ?>
                                <tr>
                                    <td>
                                        <div class="media-image d-flex">
                                            <div class="row d-flex">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <img style="width: 100px; height" src="assets/img/product/<?php echo $value['image'];?>" alt="" />
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <p><?= $value['name']; ?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h5><?= number_format($value['price']) ?><sup class="sup_new">đ</sup></h5>
                                    </td>
                                    <td>
                                        <input type="number" id="quantity_cart" name="<?= $value['id']; ?>" value="<?= $value['qty']; ?>" min="1" max="99">
                                    </td>
                                    <td>
                                        <h5>
                                            <?php
                                            $item_sum = $value['price'] * $value['qty'];
                                            echo number_format($item_sum);
                                            $_SESSION['sum_price'] += $item_sum;
                                            ?><sup class="sup_new">đ</sup>
                                        </h5>
                                    </td>
                                    <td>
                                        <button style="border:none; background: transparent;">
                                            <a href="index.php?page=cart&id=<?= $value['id']; ?>" onclick="return confirm('Bạn có muốn xóa sản phẩm này không?');" class="ti-trash btn btn-danger"></a>
                                        </button>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <h5>Subtotal</h5>
                            </td>
                            <td>
                                <h5><?php echo number_format($_SESSION['sum_price']);?><sup>đ</sup></h5>
                            </td>
                        </tr>

                        <tr class="bottom_button">
                            <td>
                                <button class="gray_btn" id="update_cart" name="update_cart">Update Cart</button>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <div class="cupon_text d-flex align-items-center">
                                    <input type="text" placeholder="Coupon Code" />
                                    <a class="primary-btn" href="#">Apply</a>
                                </div>
                            </td>
                        </tr>
                        <tr class="out_button_area">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <div class="checkout_btn_inner d-flex align-items-center">
                                    <button type="submit" name="submit_cart" class="primary-btn billing" style="border: none"><a href="index.php?page=billing" style="color: #fff;">Proceed to checkout</a></button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>
<!--================End Cart Area =================-->