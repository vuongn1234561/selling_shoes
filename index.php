<?php
    session_start();
    ob_start();
//   session_destroy();

    if(isset($_GET['page'])) {
        $page = $_GET['page'];
    }else{
        $page = 'product';
    }

    if(isset($_SESSION['login'])){
        echo '<style>#login { display:none;}</style>';
        echo '<style>#register { display:none;}</style>';
    }else{
        echo '<style>#logout { display:none;}</style>';
    }
?>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<!-- Head -->
	<?php
		include_once 'includes/head.php';
	?>
<!-- End head -->
<body>

	<!-- Start Header Area -->
	<?php
		include_once 'layout/header.php';
	?>
	<!-- End Header Area -->

	<!-- start banner Area -->
	<?php
        if($page == 'product'){
            include_once 'layout/banner.php';
            include_once 'layout/features.php';
            include_once 'layout/category.php';
        }
	?>
                <?php
                    switch ($page) {
                        case 'product':
                            include_once './Controller/products/ProductController.php';
                            $product = new ProductController;
                            break;

                        case 'cart':
                            include_once './Controller/products/CartController.php';
                            $cart = new CartController();
                            break;

                        case 'billing':
                            include_once './Controller/products/BillingController.php';
                            $billing = new BillingController();
                            break;

                        case 'login':
                            include_once './Controller/accounts/LoginController.php';
                            $login = new LoginController();
                            break;

                        case 'register':
                            include_once './Controller/accounts/RegisterController.php';
                            $register = new RegisterController();
                            break;

                        case 'logout':
                            include_once './Controller/accounts/LogoutController.php';
                            $logout = new LogoutController();
                            break;

                        case 'confirmation':
                            include_once './Controller/products/ConfirmationController.php';
                            $confirmation = new ConfirmationController();
                            break;

                        default:
                            echo 'Page 404 not found';
                            break;
                    }
                ?>
	<?php
        if($page == 'product'){
            include_once 'layout/deal.php';
            include_once 'layout/brand.php';
            include_once 'layout/related-product.php';
        }
	?>

	<?php
		include_once 'layout/footer.php';
	?>
	<!-- End footer Area -->

	<!-- Script -->
	<?php
		include_once 'includes/script.php';
	?>
	<!-- End script -->
</body>

</html>